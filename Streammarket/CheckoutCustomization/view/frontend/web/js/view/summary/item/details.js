/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'uiComponent'
    ],
    function (Component) {
        "use strict";
        var quoteItemData = window.checkoutConfig.quoteItemData;
        return Component.extend({
            defaults: {
                template: 'Streammarket_CheckoutCustomization/summary/item/details'
            },
            quoteItemData: quoteItemData,
            getValue: function(quoteItem) {
                return quoteItem.name;
            },
            /* Display product weight code*/
            getWeight: function(quoteItem) {
                var item = this.getItem(quoteItem.item_id);
                return item.weight;
            },
            getItem: function(item_id) {
                var itemElement = null;
                _.each(this.quoteItemData, function(element, index) {
                    if (element.item_id === item_id) {
                        itemElement = element;
                    }
                });
                return itemElement;
            }
        });
    }
);